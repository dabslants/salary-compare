/**
 * Salary Compare DB Query
 *
 * Tables: rates
 *
 * @author  Oluwaseyi Sobande
 * @email   dabslants@gmail.com
 */

CREATE DATABASE IF NOT EXISTS ratrace0318
    DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE ratrace0318;

/**
 * Currency Rates against 1 dollar
 */
CREATE TABLE IF NOT EXISTS rates(
    id int PRIMARY KEY AUTO_INCREMENT NOT NULL,
    rates text NOT NULL,
    created timestamp DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
