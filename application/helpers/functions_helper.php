<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* CodeIgniter General Helpers
*
* @package		OK Framework
* @subpackage	Helpers
* @category		Helpers
* @author		QP Dev Team
* @link
*/

// ------------------------------------------------------------------------

if (!function_exists('dd')) {
    /**
     * Display var_dump() and die();
     */
    function dd() {
        call_user_func_array('var_dump', func_get_args());
        die();
    }
}

if (!function_exists('get_ip_info')) {
    /**
     * Get API Info
     * @param  string $ip IP Address
     * @return string     json of api info
     */
    function get_ip_info($ip = NULL) {
        if(empty($ip)) { $ip = $_SERVER['REMOTE_ADDR']; }
        return json_decode(@file_get_contents("http://ipinfo.io/$ip/json"), true);
    }
}

if (!function_exists('ok_error')) {
    /**
     * Display session message
     * @param  string|array $msgs session message
     */
    function ok_error($msgs) {
        if (!empty($msgs)) {
            $type = 'error';
            echo '<div class="message">';
            if (is_array($msgs)) {
                foreach ($msgs as $key => $val) {
                    if (is_array($val)) {
                        $type = empty($val[1]) ? $type : $val[1];
                        $val = $val[0];
                    }
                    echo "<p class=\"{$type}\">{$val}</p>";
                }
            } else {
                $msgs = trim($msgs);
                echo "<p class=\"{$type}\">{$msgs}</p>";
            }
            echo '</div>';
        }
    }
}

if (!function_exists('currency_dollar_rate')) {
    /**
     * Currency conversion usinf Currency Layer API. Default is USD
     * @return json
     */
    function currency_dollar_rate() {
        // http://www.apilayer.net/api/live?access_key=d14a43470dad8a5992facfd018826540&source=USD
        $link = sprintf("http://www.apilayer.net/api/live?access_key=%s&source=%s"
            , CL_API_KEY, CL_SOURCE);
        $response = @file_get_contents($link);
        $response = json_decode($response, true);

        if (!empty($response['success']) && $response['success'] == true) {
            $rates = ['NGN'=>$response['quotes']['USDNGN'], 'EUR'=>$response['quotes']['USDEUR'], 'GBP'=>$response['quotes']['USDGBP']];

            // save rates to db
            $ci =& get_instance();
            $ci->db->insert(CL_TABLE, ['rates'=>json_encode($rates)]);

            return $rates;
        }
    }
}

if (!function_exists('currency_symbol')) {
    /**
     * Currency symbol
     *
     * @param  string $curr currency
     * @return string hexdecimal representation
     */
    function currency_symbol($curr='naira') {
        switch ($curr) {
            case 'dollar':
                return' &#x24;';

            case 'euro':
                return '&#x20AC;';

            case 'pound':
                return '&#xa3;';

            default:
                return '&#x20A6;';
        }
    }
}

if (!function_exists('convert_to_curr')) {
    /**
     * Convert currency
     *
     * @param  int $amount amount to convert in dollar
     * @return int converted sum
     */
    function convert_to_curr($amount, $rates, $curr='naira') {
        switch ($curr) {
            case 'dollar':
                return convert_to_dollar($amount, $rates);

            case 'euro':
                return convert_to_euro($amount, $rates);

            case 'pound':
                return convert_to_pound($amount, $rates);

            default:
                return $amount;
        }
    }
}

if (!function_exists('convert_from_curr')) {
    /**
     * Convert currency
     *
     * @param  int $amount amount to convert in dollar
     * @return int converted sum
     */
    function convert_from_curr($amount, $rates, $curr='naira') {
        switch ($curr) {
            case 'dollar':
                return convert_to_naira($amount, $rates);

            case 'euro':
                return ($amount / $rates['EUR']) * $rates['NGN'];

            case 'pound':
                return ($amount / $rates['GBP']) * $rates['NGN'];

            default:
                return $amount;
        }
    }
}

if (!function_exists('convert_to_naira')) {
    /**
     * Convert currency from dollar to naira
     * rates is assumed against 1 dollar
     *
     * @param  int $amount amount to convert in dollar
     * @param  array $rates
     * @return int converted sum
     */
    function convert_to_naira($amount, $rates) {
        return ($amount * $rates['NGN']);
    }
}

if (!function_exists('convert_to_dollar')) {
    /**
     * Convert currency from naira to dollar
     * rates is assumed against 1 dollar
     *
     * @param  int $amount amount to convert in dollar
     * @param  array $rates
     * @return int converted sum
     */
    function convert_to_dollar($amount, $rates) {
        return ($amount / $rates['NGN']);
    }
}

if (!function_exists('convert_to_euro')) {
    /**
     * Convert currency from naira to dollars then to euros
     * rates is assumed against 1 dollar
     *
     * @param  int $amount amount to convert in naira
     * @param  array $rates
     * @return int converted sum
     */
    function convert_to_euro($amount, $rates) {
        return convert_to_dollar($amount, $rates) * $rates['EUR'];
    }
}

if (!function_exists('convert_to_pound')) {
    /**
     * Convert currency from naira to dollars then to euros
     * rates is assumed against 1 dollar
     *
     * @param  int $amount amount to convert in naira
     * @param  array $rates
     * @return int converted sum
     */
    function convert_to_pound($amount, $rates) {
        return convert_to_dollar($amount, $rates) * $rates['GBP'];
    }
}

if (!function_exists('form_error')) {
    /**
     * Convert string form error to array
     * @param  string $error for error
     * @return array        from error
     */
    function form_error($error) {
        return explode("\n", trim(strip_tags($error)));
    }
}
