<?php require APPPATH.'views/template/header.php' ?>
                        <h2>Average Annual salary across the world</h2>
                        <?php foreach ($world as $continent => $countries): ?>
                            <h3><?php echo $continent; ?></h3>
                            <ul>
                                <?php foreach ($countries as $country => $salary): ?>
                                    <li><span class="key"><?php echo $country; ?></span><span class="sal">  earns <?php echo currency_symbol($this->session->currency); ?>
                                        <?php echo !empty($result) ? number_format(convert_to_curr($result['diff'][$continent][$country], $rates, $this->session->currency), 2,'.', ',') : number_format(convert_to_curr($salary, $rates, $this->session->currency), 2,'.', ','); ?>
                                    </span> <span class="info">
                                        <?php echo !empty($result) ? "earns ".$result['compare'][$continent][$country] : 'per year'; ?>
                                    </span></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endforeach; ?>
<?php require APPPATH.'views/template/footer.php' ?>
