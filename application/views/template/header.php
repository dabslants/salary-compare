<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="keyword" content="Nigeria, Nigerian Senator, Monthly Salary, Omoyele Sowore, Sowore2019, Sowore 2019, TakeItBack">
        <meta name="description" content="Salary Compare Tool. This tool compare your salary to that of a Nigerian senator.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <title>Salary Calculator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/normalize.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css'); ?>">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    </head>
    <body>
        <div id="page">
            <header class="logo">
                <p><img src="<?php echo base_url('assets/images/take-it-back.png'); ?>" alt="TakeItBack logo"></p>
            </header>
            <section>
                <div id="group">

                    <?php ok_error($this->session->error); ?>

                    <div id="left">
                        <header>
                            <h1>Salary Compare Tool</h1>
                            <p>This tool compare your salary to that of a Nigerian senator</p>
                        </header>
                        <form class="senator-compare" action="compare/senator" method="post">
                            <h3 class="form-heading">Enter your salary per month</h3>
                            <p class="from-group">
                                <label for="salary">&#x20A6;</label>
                                <input type="text" id="salary" name="salary" placeholder="Example: 50000" required="required" class="wide-field table-cell">
                                <select class="" name="currency">
                                    <option value="dollar">Dollar</option>
                                    <option value="euro">Euro</option>
                                    <option value="pound">Pound</option>
                                    <option value="naira" selected>Naira</option>
                                </select>
                            </p>
                            <input type="submit" name="submit" value="Compare with a Nigerian Senator">
                        </form>
                    </div>
                    <div id="right">
                        <?php ok_error($this->session->message); ?>
