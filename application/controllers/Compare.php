<?php
/**
 * Salary Compare Tool. This tool compare your salary to that of a Nigerian senator.
 * @author  Dabs Lants<dabslants@gmail.com>
 * @version v1.0.0
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Compare extends CI_Controller {

    /**
     * World annual salary in dollars
     * @var array
     */
    private $world = [
                    'Africa'=>['South Africa'=>47200, 'Ghana'=>21401, 'Kenya'=>24962, 'Niger'=>24314, 'Zambia'=>18000, 'Mauritius'=>29255, 'Morocco'=>39417],
                    'America'=>['USA'=>79936, 'Canada'=>70602, 'Mexico'=>48018, 'Brazil'=>58808],
                    'Europe'=>['United Kingdom'=>74000]
                ];

                /**
                 * Naira to Dollar rate
                 * @var decimal
                 */
    private $rates = [];

    function __construct() {
        parent::__construct();
        $this->load->helper(['form', 'url']);
        $this->load->library(['form_validation']);
        $this->load->model(['compare_model'=>'cm']);

        $this->rates = $this->cm->get_rate();
        $this->_salary_to_naira();
    }

	public function index() {
        $data['world'] = $this->world;
        $data['rates'] = $this->rates;
        $data['result'] = $this->session->result;
        $data['currency'] = !empty($this->session->currency) ? $this->session->currency : 'naira';
		$this->load->view('senator-compare/compare', $data);
	}

    /**
     * Compare monthly salary to a Nigerian Senator monthly salary
     */
    public function month() {
        $this->form_validation->set_rules('salary', 'Your Monthly Salary is required', 'trim|required|numeric');
        $this->form_validation->set_rules('currency', 'Please choose a currency', 'trim|required|alpha');

        if ($this->form_validation->run() == FALSE) {
            $this->session->error = explode("\n", trim(strip_tags(validation_errors())));
            $this->session->mark_as_flash('error');
            redirect('compare', 'location');

        } else {
            $salary = $this->input->post('salary');
            $currency = $this->input->post('currency');

            // convert to naira
            if ($currency != 'naira') {
                $salary = convert_from_curr($salary, $this->rates, $currency);
            }

            $senator_monthly_earnings = 13500000;
            $time_to_earn = $senator_monthly_earnings / $salary;
            $time_to_earn_year = ($senator_monthly_earnings*12) / ($salary*12);
            $msg = '';

            if ($salary > $senator_monthly_earnings) {

                $msg = [["Congratulations! Your earn more than a Nigeria Senator.", 'sucess']];
            } elseif ($salary == $senator_monthly_earnings) {

                $msg = [["Wow! You earn the same a Nigeria Senator.", 'sucess']];
            } else {

                $msg = [[sprintf("It will take you %s to earn a Nigeria Senator's salary", $this->_format_time($time_to_earn)), 'sucess']];
                // $msg = [[sprintf("It will take you %s to earn a month salary of a Nigeria Senator", $this->_format_time($time_to_earn)), 'sucess'],[sprintf("It will take you %s to earn a years salary of a Nigeria Senator", $this->_format_time($time_to_earn_year)), 'sucess']];
            }

            $this->session->message = $msg;
            $this->session->result = ['compare'=>$this->_compare_world($salary), 'diff'=>$this->_difference_world($salary)];
            $this->session->currency = $currency;
            $this->session->salary = $salary;
            $this->session->mark_as_flash('result');
            $this->session->mark_as_flash('message');
            redirect('compare#right', 'location');
        }
    }

    private function _salary_to_naira() {
        foreach ($this->world as $continent => $countries) {
            foreach ($countries as $country => $salary) {
                $this->world[$continent][$country] = convert_to_naira($salary, $this->rates);
            }
        }
    }

    private function _format_time($time) {
        $period='';
        // years and months
        if ($time < 12) {
            // month
            $m = ceil($time);
            $period = $m > 1 ? $m.' months' : $m.' month';
        } else {
            if (($time % 12) == 0) {
                // year
                $y = $time/12;
                $period = $y > 1 ? $y.' years' : $y.' year';
            } else {
                // year
                $y = floor($time/12);
                $period = $y > 1 ? $y.' years ' : $y.' year ';

                // month
                $m = $time%12;
                $period .= $m > 1 ? $m.' months' : $m.' month';
            }
        }
        return $period;
    }

    private function _compare_world($s1) {
        $compare = [];
        foreach ($this->world as $continent => $countries) {
            foreach ($countries as $country => $s2) {
                // $s1 *= 12;  // to year
                // $s2 *= $this->rate; // to naira
                $c = $this->_compare_salary($s1*12, $s2);//$s2*$this->rate);
                $compare[$continent][$country] = $this->_compare_info($c);
            }
        }
        return $compare;
    }

    private function _difference_world($s1) {
        $diff = [];
        foreach ($this->world as $continent => $countries) {
            foreach ($countries as $country => $s2) {
                $d1 = $s1 * 12;  // to year
                $d2 = $s2;//$s2 * $this->rate; // to naira
                $diff[$continent][$country] = $d2 > $d1 ? $d2-$d1 : $d1-$d2;//($s2 * $this->rate) - ($s1 * 12) : ($s1 * 12) - ($s2 * $this->rate);
            }
        }
        return $diff;
    }

    private function _compare_salary($s1, $s2) {
        if ($s1 < $s2) {
            return -1;
        }
        if ($s1 > $s2) {
            return 1;
        }
        return 0;
    }

    private function _compare_info($c) {
        switch ($c) {
            case -1:
                return 'more than you in a year';
            case 0:
                return 'same as you in a year';
            case 1:
                return 'less than you in a year';
        }
    }
}
