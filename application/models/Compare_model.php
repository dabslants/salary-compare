<?php

class Compare_model extends CI_Model {

    // table name
    protected $table;

    // hold database result
    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->table = CL_TABLE;
    }

    public function get_rate() {
        $this->db->select('rates');
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $result = $this->db->get('rates');
        $result = $result->result();

        if (count($result) == 0 || explode(' ',$result[0]->created, 2)[0] != date('Y-m-d')) {
            $this->data = currency_dollar_rate();
        } else {
            $this->data = json_decode($result[0]->rates, true);
        }
        return $this->data;
    }

}
