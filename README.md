# Salary Calculator

Find out how long it will take you to earn a nigerian senator's monthly salary.

## Configuration
- Set Database username & password in 'application/config/database.php' on line 79 & 80
- Currency Layer(https://currencylayer.com/) API was used. To add your API key, goto 'application/helpers/constants_helper.php'. Set CL_API_KEY to your API key
